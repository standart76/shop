FROM openjdk:8
EXPOSE 8081
ADD /target/demo-0.1.jar demo-0.1.jar
ENTRYPOINT ["java","-jar","demo-0.1.jar"]