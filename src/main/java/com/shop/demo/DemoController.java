package com.shop.demo;

import com.shop.demo.Entity.Item;
import com.shop.demo.Entity.User;
import com.shop.demo.Repo.ItemRepo;

import com.shop.demo.Repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
public class DemoController {

    @Autowired
    ItemRepo itemRepo;

    @Autowired
    UserRepo userRepo;

    @GetMapping("/")//главная страница
    public String DemoMainPage( Model model) {
        model.addAttribute("items", itemRepo.findAll());
        return "index";
    }

    @GetMapping("/admin")//панель администратора
    public String DemoAdminPage( Model model) {
        model.addAttribute("items", itemRepo.findAll());
        model.addAttribute("users", itemRepo.findAll());
        model.addAttribute("orders", itemRepo.findAll());
        return "admin";
    }

    @GetMapping("/items")//список всех товаров
    public String DemoItemsPage(Model model) {
        model.addAttribute("items", itemRepo.findAll());
        return "items";
    }

    @GetMapping("/add")//добавление товара
    public String DemoAddPage(Model model) {
        return "addItem";
    }

    @RequestMapping(value="/add", method=RequestMethod.POST)//сохранение нового товара
    public String DemoAddPageSave(@ModelAttribute Item item,Model model){
        itemRepo.save(item);
        return "redirect:/items";
    }

    @RequestMapping(value = {"/edit/{id}"},method = RequestMethod.GET)//изменение товара
    public String DemoEditPage(Model model,@PathVariable Integer id) {
        model.addAttribute("editItem",itemRepo.findById(id).get());
        return "editItem";
    }
    @RequestMapping(value = {"/edit/{id}"},method = RequestMethod.POST)//сохранение изменения товара
    public String DemoEditPageSave(@ModelAttribute Item item,Model model) {
        itemRepo.save(item);
        return "redirect:/items";
    }
    @RequestMapping(value = {"/delete/{id}"},method = RequestMethod.GET)//удаление товара
    public String DemoEditPageDelete(@ModelAttribute Item item,Model model) {
        itemRepo.delete(item);
        return "redirect:/items";
    }

    @RequestMapping(value = {"/viewItem/{id}"},method = RequestMethod.GET)//карточка товара
    public String viewItem(Model model, @PathVariable Integer id){
        model.addAttribute("findItem", itemRepo.findById(id).get());
        return "item";
    }

    @GetMapping("/card")//корзина товаров
    public String DemoCardPage(Model model) {
        return "card";
    }

    @PostMapping(value = "/login")//после авторизации
    public String loginSubmit(@ModelAttribute User user,Model model,HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("uri", request.getRequestURI())
                .addAttribute("user", auth.getName())
                .addAttribute("roles", auth.getAuthorities());
        userRepo.getUserByUname(user.getUname());
        return "redirect:/";
    }


    @GetMapping(value = "/login")//страница авторизации
    public String authPage() {
        return "login";
    }

    @RequestMapping(value = "/newuser", method = RequestMethod.GET)//создание пользователя
    public String newUserPage(Model model) {
        return "newuser";
    }

    @PostMapping(value="/createuser")//сохранение нового товара
    public String DemoAddUser(@ModelAttribute User user, Model model){
        userRepo.save(user);
        return "redirect:/";
    }

    @RequestMapping(value="/logout", method = RequestMethod.POST)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }





}
