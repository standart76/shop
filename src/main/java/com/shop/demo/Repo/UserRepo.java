package com.shop.demo.Repo;

import com.shop.demo.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface UserRepo extends JpaRepository<User, String> {
   User getUserByUname(String uname);
   Optional<User> findOneByUname(String uname);

}
