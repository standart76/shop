package com.shop.demo;

import com.shop.demo.Entity.User;
import com.shop.demo.Repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Column;
import java.util.Collection;


public class UserService implements UserDetails {
    @Autowired
    UserRepo userRepository;
    private Integer  id;
    private String uname;
    private String email;
    private String password;
    private String role;

    public UserService(Integer id, String uname, String password, String email, String role) {
        this.id=id;
        this.uname=uname;
        this.password=password;
        this.email=email;
        this.role=role;
    }

    public User findOne(String uname) {
        return userRepository.getUserByUname(uname);
    }

    public static UserService build(User user) {
        return new UserService(
            user.getId(),
            user.getUname(),
            user.getPassword(),
            user.getEmail(),
            user.getRole()
        );
    }

    @Transactional
    public User save(User user) {
        user.setPassword(user.getPassword());
        User savedUser = userRepository.save(user);
        return userRepository.save(savedUser);
    }

    @Transactional
    public User update(User user) {
        User oldUser = userRepository.getUserByUname(user.getUname());
        oldUser.setPassword(user.getPassword());
        oldUser.setUname(user.getUname());
        return userRepository.save(oldUser);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}