package com.shop.demo.Entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name="items")
@Getter
@Setter
@ToString
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer  id;

    @Column
    private String name;

    @Column
    private Double price;

    @Column
    private String img;

}
