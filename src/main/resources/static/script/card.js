document.addEventListener('DOMContentLoaded', () => {
    'use strict';
    const card = []; //объект для корзины
    const cardItems = document.querySelector('.cardItems');
    const cardTotal = document.querySelector('.cardTotal');
    const buyBtn = document.getElementById('buyBtn');
    const delBtn = document.getElementById('delBtn');
    const cont= document.querySelector(".content");
    const drawCard = items=>{
        cardItems.textContent='';
        if(items.length){
            let sum=0;
            items.forEach(item=>{
                cardItems.innerHTML+=`
                <tr>
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.price}</td>
                    <td>
<button class="btn btn-primary counter-button counter-minus" data-id="${item.id}">-</button>
<button class="btn btn-primary counter-button counter-plus" data-id="${item.id}">+</button>
<span class="counter">${item.count}</span>

</td>
                    <td>${item.price*item.count}</td>
                </tr>`;
                sum+=item.price*item.count;
            });
            cardTotal.innerHTML=sum;
        }
        else{
            //корзина пустая
            cardItems.innerHTML=`<tr><td colspan="5"><h3>корзина пуста</h3></td></tr>`;
            cardTotal.innerHTML=0;
        }
    };
    // Извлечение и сохраниение id товаров из localStorage
    const storageQuery = get => {
        if (get) {
            if (localStorage.getItem('items')) {
                const itemsStorage = JSON.parse(localStorage.getItem('items'));
                card.push(...itemsStorage); // массив преобразуем в набор элементов
                drawCard(card);
            }
        } else {
            localStorage.setItem('items', JSON.stringify(card));
        }
    };

    const finishOrder =()=>{//переписать, отправить данные на серв
        localStorage.clear();
        card.length=0;
        drawCard(card);
    };

    const clearCart =()=>{
        localStorage.clear();
        card.length=0;
        drawCard(card);
    };

    function changeCount(event){
        const target = event.target;
        if(target.classList.contains('counter-button')){
            const food = card.find(function(item){
                return item.id === target.dataset.id;
            });
            if(target.classList.contains('counter-minus')){
                food.count--;
                if(food.count===0){
                    card.splice(card.indexOf(food),1);
                }
            }
            if(target.classList.contains('counter-plus')){
                food.count++;
            }
            storageQuery();
            drawCard(card);
        }
    }

    buyBtn.addEventListener('click',finishOrder);
    delBtn.addEventListener('click',clearCart);
    cont.addEventListener('click',changeCount);
    storageQuery(true);


});