document.addEventListener('DOMContentLoaded', () => {
    'use strict';
    const card = []; //объект для корзины
    const cardItems = document.querySelector('.cardItems');
    const cardTotal = document.querySelector('.cardTotal');
    const buyBtn = document.getElementById('buyBtn');


    const drawCard = items=>{
        if(items.length){
            let sum=0;
            items.forEach(item=>{
                cardItems.innerHTML+=`
                <tr>
                    <td>${item.id}</td>
                    <td>${item.name}</td>
                    <td>${item.price}</td>
                    <td>${item.count}</td>
                    <td>${item.price*item.count}</td>
                </tr>`;
                sum+=item.price*item.count;
            });
            cardTotal.innerHTML=sum;
        }
        else{
            //корзина пустая
            cardItems.innerHTML=`<tr><td colspan="5"><h3>корзина пуста</h3></td></tr>`;
            cardTotal.innerHTML=0;
        }
    };
    // Извлечение и сохраниение id понравившиеся товары из localStorage
    const storageQuery = get => {
        if (get) {
            if (localStorage.getItem('items')) {
                const itemsStorage = JSON.parse(localStorage.getItem('items'));
                card.push(...itemsStorage); // массив преобразуем в набор элементов
                drawCard(card);
            }
        } else {
            localStorage.setItem('items', JSON.stringify(card));
        }
    };

    const finishOrder =()=>{
        localStorage.clear();
        card.length=0;
        drawCard(card);
    };

    buyBtn.addEventListener('click',finishOrder);
    storageQuery(true);


});