document.addEventListener('DOMContentLoaded', () => {
    'use strict';


    const mainDiv = document.querySelector('.row');
    const cardCounter = document.querySelector('.counter');
    const card = []; //объект для корзины


    // Извлечение и сохраниение id понравившиеся товары из localStorage
    const storageQuery = get => {
        if (get) {
            if (localStorage.getItem('items')) {
                const itemsStorage = JSON.parse(localStorage.getItem('items'));
                card.push(...itemsStorage); // массив преобразуем в набор элементов
            }
            renderCount();
        } else {
            localStorage.setItem('items', JSON.stringify(card));
        }
    }


    const handlerItems = event => {
        const target = event.target;
        let found=0;
        if (target.textContent === 'Купить') {
            const parent = target.parentElement;
            if(card.length===0){
                let goods={};//нет элементов, записываем первый
                goods.id=target.dataset.itemsId;
                goods.price = parent.querySelector('.price').textContent;
                goods.name = parent.querySelector('.card-title').textContent;
                goods.count=1;
                card.push(goods);
            }
            else{
                for(let item of card){
                    if(item.id===target.dataset.itemsId){//если есть такой товар, добавляем количество
                        item.count++;
                        found=1;
                    }
                }
                if(!found){//если не нашли, добавляем товар
                    let goods={};
                    goods.id=target.dataset.itemsId;
                    goods.price = parent.querySelector('.price').textContent;
                    goods.name = parent.querySelector('.card-title').textContent;
                    goods.count=1;
                    card.push(goods);
                }
            }
            storageQuery();
            renderCount();
        }
    }

    // Количество товаров в корзине
    const renderCount = () => {
        cardCounter.textContent = card.length;
    }

    mainDiv?mainDiv.addEventListener('click', handlerItems):'';
    storageQuery(true);// - извлечение из localStorage id товаров

});